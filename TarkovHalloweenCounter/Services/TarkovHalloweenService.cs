﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TarkovHalloweenCounter.Services
{
    public static class TarkovHalloweenService
    {
        private static readonly Uri TarkovUri = new Uri("https://www.escapefromtarkov.com");

        public static async Task<int> GetCurrentCount()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = TarkovUri;
                var result = await client.GetAsync("/halloween");
                var stringContent = await result.Content.ReadAsStringAsync();

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(stringContent);

                var counters = doc.QuerySelector(".count");
                var correctChildNode = counters.ChildNodes.ElementAt(1);

                int.TryParse(correctChildNode?.InnerText ?? "0", out var actualCount);
                return actualCount;
            }
        }
    }
}
