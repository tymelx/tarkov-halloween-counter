﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TarkovHalloweenCounter.Services;

namespace TarkovHalloweenCounter
{
    public partial class Form1 : Form
    {
        private static readonly string _fileName = "tarkov_halloween_counter.txt";
        private string _outputPath = string.Empty;
        private int _defaultIntervalInSeconds = 30;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnOutputDirectory_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    lblChosenOutputDirectory.Text = fbd.SelectedPath;
                    _outputPath = $"{fbd.SelectedPath}\\{_fileName}";
                    //fbd.SelectedPath
                }
            }
        }

        private async void btnStart_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_outputPath))
            {
                MessageBox.Show("You must select an output directory before starting");
                return;
            }

            //logic to start everything up
            if (!string.IsNullOrEmpty(txtRefreshInterval.Text) && int.TryParse(txtRefreshInterval.Text, out var interval))
                _defaultIntervalInSeconds = interval;

            await CreateCountFile();
            tmrGetCount.Interval = _defaultIntervalInSeconds * 1000;
            tmrGetCount.Start();

            btnStart.Enabled = false;
            txtRefreshInterval.Enabled = false;
            btnStop.Enabled = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            //logic to stop the counter
            tmrGetCount.Stop();

            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }

        private async void tmrGetCount_Tick(object sender, EventArgs e)
        {
            await CreateCountFile();
        }

        private async Task CreateCountFile()
        {
            var actualCount = await TarkovHalloweenService.GetCurrentCount();
            System.IO.File.WriteAllText(_outputPath, actualCount.ToString());
        }
    }
}
